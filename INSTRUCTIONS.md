**Kindly read the instructions carefully. You have 48 hours to submit your output for this exercise.**

Instructions: 

**Step 1:**
Create a single-page website with an appointment booking for pet services using your preferred back-end framework. Follow the mock-up design provided.

The website should fulfill the following requirements:

- As a user, I should be able to see a full banner upon entering the site
- As a user, I should be able to see more details about the service
- As a user, I should be able to schedule an appointment
- The data should be stored and updated in a database (SQL)

Mock-up: https://gitlab.com/sven-exams/web-developer-exam/blob/master/Marketing_site.png

Slices: https://gitlab.com/sven-exams/web-developer-exam

Fonts: Visit the following URLs to sync the Basic Sans and Henriette typefaces through Typekit.
https://typekit.com/fonts/basic-sans
https://typekit.com/fonts/henriette

**Step 2:**
Put your source codes in a project folder with the following format: 
web-firstnamelastname, ie: web-danieljosephtan

**Step 3:**
Once you are done, zip your project folder and send it via email to: mlua@svengroup.com, fvallente@svengroup.com, mlumapas@svengroup.com, kempleo@svengroup.com